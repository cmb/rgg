
#include "cmbNucPartsTreeItem.h"
#include "cmbNucAssembly.h"
#include "cmbNucCore.h"
#include <QFileInfo>
#include <QFont>
#include <QBrush>
#include <QStyle>
#include <QIcon>

namespace
{
  
}

//-----------------------------------------------------------------------------
cmbNucPartsTreeItem::cmbNucPartsTreeItem(QTreeWidgetItem* pNode, cmbNucPart* obj, QString label)
:  QTreeWidgetItem(pNode), PartObject(obj)
{
  this->connection = new cmbNucPartsTreeItemConnection();
  this->connection->v = this;
  this->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
  this->setChildIndicatorPolicy(QTreeWidgetItem::DontShowIndicatorWhenChildless);
  this->setExpanded(false);
  this->setText(3, "");
  toolTipMode[0] = toolTipMode[1] = toolTipMode[2]  = toolTipMode[3] = NO_TOOL_TIP;
  this->setTextAlignment(1,Qt::AlignHCenter|Qt::AlignVCenter);
  this->setTextAlignment(2,Qt::AlignHCenter|Qt::AlignVCenter);
  QFont f = font(1);
  f.setPixelSize(20);
  this->setFont(1, f);
  f = font(2);
  f.setPixelSize(20);
  this->setFont(2, f);

  if(this->PartObject != NULL)
  {
    this->connection->objectChanged(NULL);
    QObject::connect(this->PartObject->getConnection(), SIGNAL(partChanged(cmbNucPart*)),
                     this->connection,                  SLOT(objectChanged(cmbNucPart*)));
    QObject::connect(this->PartObject->getConnection(), SIGNAL(checkChangeState()),
                     this->connection,                  SLOT(checkSaveAndGenerate()));
  }
  else
  {
    this->setText(0, label);
  }
}

//-----------------------------------------------------------------------------
cmbNucPartsTreeItem::~cmbNucPartsTreeItem()
{
  delete this->connection;
}
//-----------------------------------------------------------------------------

void cmbNucPartsTreeItemConnection::checkSaveAndGenerate()
{
  v->checkSaveAndGenerate();
}

void cmbNucPartsTreeItemConnection::objectChanged(cmbNucPart*)
{
  v->setText(0, v->PartObject->getListTitle());
  v->checkSaveAndGenerate();
}

static const QChar glyphSave[2]     = {QChar(9999), QChar(0x25FC)};
static const QChar glyphGenerate[2] = {QChar(10007), QChar(0x25FC)};

void cmbNucPartsTreeItem::checkSaveAndGenerate()
{
  enumNucPartsType selType = PartObject->GetType();
  if(selType == CMBNUC_CORE)
  {
    cmbNucCore * core = dynamic_cast<cmbNucCore*>(PartObject);
    this->setHighlight(1, core->changeSinceLastSave(), glyphSave);
    this->setHighlight(2, core->changeSinceLastGenerate(), glyphGenerate);
  }
  else if( selType == CMBNUC_ASSEMBLY)
  {
    cmbNucAssembly * assy = dynamic_cast<cmbNucAssembly*>(PartObject);
    this->setHighlight(2, assy->changeSinceLastGenerate(), glyphGenerate);
  }

  std::string fname = this->PartObject->getFileName();
  if(fname.empty())
  {
    this->toolTipMode[3] = cmbNucPartsTreeItem::NO_TOOL_TIP;
    this->setText(3, "");
  }
  else
  {
    this->toolTipMode[3] = cmbNucPartsTreeItem::OK;
    this->fileInfo = QFileInfo(fname.c_str());
    this->setText(3, this->fileInfo.fileName());
  }
}

void cmbNucPartsTreeItem::setHighlight(int index, bool b, QChar const glyph[2])
{
  static QBrush brush[2] = {QBrush(), QBrush(Qt::green)};
  this->toolTipMode[index] = (b)?NEED_ACTION:OK;
  this->setText( index, glyph[this->toolTipMode[index]-1] );
  this->setForeground( index , brush[this->toolTipMode[index]-1] );
}

QVariant cmbNucPartsTreeItem::data( int index, int role ) const
{
  if(toolTipMode[index] == NO_TOOL_TIP || role != Qt::ToolTipRole)
    return this->QTreeWidgetItem::data(index, role);

  if( index == 3) return QVariant(fileInfo.absoluteFilePath());
  static QVariant messages[2][2] = {{QVariant("Needs to be saved"),QVariant("Saved")},
                        {QVariant("MeshKit needs to be run"),QVariant("MeshKit files up to date")}};
  qDebug() << index -1 << toolTipMode[index]-1 << messages[index-1][toolTipMode[index]-1];
  return messages[index-1][toolTipMode[index]-1];
}
