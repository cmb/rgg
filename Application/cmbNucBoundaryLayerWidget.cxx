#include "cmbNucBoundaryLayerWidget.h"
#include "ui_qBoundaryLayerWidget.h"

#include "cmbNucMaterialColors.h"

class cmbNucBoundaryLayerWidgetInternal: public Ui::boundaryLayerWidget
{
public:
};

cmbNucBoundaryLayerWidget::cmbNucBoundaryLayerWidget(QWidget * p)
: cmbNucCheckableWidget(p), Internal(new cmbNucBoundaryLayerWidgetInternal())
{
  this->Core = NULL;
  this->Internal->setupUi(this);
  this->setBoundaryEnabled(false);
  QObject::connect( this->Internal->boundaryLayer,     SIGNAL(clicked(bool)),
                    this,                              SLOT(checkChange()) );
  QObject::connect( this->Internal->boundaryLayer,     SIGNAL(clicked(bool)),
                    this,                              SLOT(setBoundaryEnabled(bool)) );
  QObject::connect( this->Internal->boundaryMaterials, SIGNAL(currentIndexChanged(const QString &)),
                    this,                              SLOT(checkChange()) );
  QObject::connect( this->Internal->interval,          SIGNAL(valueChanged(int)),
                    this,                              SLOT(checkChange()) );
  QObject::connect( this->Internal->thickness,         SIGNAL(valueChanged(double)),
                    this,                              SLOT(checkChange()) );
  QObject::connect( this->Internal->bias,              SIGNAL(valueChanged(double)),
                    this,                              SLOT(checkChange()) );
}

cmbNucBoundaryLayerWidget::~cmbNucBoundaryLayerWidget()
{
  delete Internal;
}

void cmbNucBoundaryLayerWidget::set(cmbNucCore * core)
{
  this->Core = core;
  onReset();
}

void cmbNucBoundaryLayerWidget::onApply()
{
  if(this->Core == NULL) return;

  if(this->Internal->boundaryLayer->isChecked() )
  {
    cmbNucCore::boundaryLayer * bl = NULL;
    if(this->Core->getNumberOfBoundaryLayers() != 0)
    {
      bl = this->Core->getBoundaryLayer(0);
    }
    else
    {
      bl = new cmbNucCore::boundaryLayer();
      this->Core->addBoundaryLayer(bl);
      this->Core->boundaryLayerChanged();
    }
    double tmpbias = this->Internal->bias->value();
    double tmpthichness = this->Internal->thickness->value();
    int tmpinterval = this->Internal->interval->value();
    QPointer<cmbNucMaterial> tmpmat =
                  cmbNucMaterialColors::instance()->getMaterial(this->Internal->boundaryMaterials);
    if(tmpbias != bl->Bias || tmpthichness != bl->Thickness ||
       tmpinterval != bl->Intervals || tmpmat != bl->interface_material)
    {
      bl->Bias = tmpbias;
      bl->Thickness = tmpthichness;
      bl->Intervals = tmpinterval;
      bl->interface_material = tmpmat;
      this->Core->boundaryLayerChanged();
    }
  }
  else if(this->Core->getNumberOfBoundaryLayers() != 0)
  {
    this->Core->clearBoundaryLayer();
    this->Core->boundaryLayerChanged();
  }
  if(isDifferent())
  {
    this->Core->setAndTestDiffFromFiles(isDifferent());
    this->Core->getConnection()->componentChanged(NULL);
  }
}

void cmbNucBoundaryLayerWidget::onReset()
{
  cmbNucMaterialColors* matColorMap = cmbNucMaterialColors::instance();
  matColorMap->setUp(this->Internal->boundaryMaterials);

  if(this->Core == NULL) return;

  if(this->Core->getNumberOfBoundaryLayers() != 0)
  {
    this->Internal->boundaryLayer->setChecked(true);
    setBoundaryEnabled(true);
    cmbNucCore::boundaryLayer * bl = this->Core->getBoundaryLayer(0);
    this->Internal->bias->setValue(bl->Bias);
    this->Internal->thickness->setValue(bl->Thickness);
    this->Internal->interval->setValue(bl->Intervals);
    cmbNucMaterialColors::instance()->selectIndex(this->Internal->boundaryMaterials,
                                                  bl->interface_material);
  }
  else
  {
    this->Internal->boundaryLayer->setChecked(false);
    setBoundaryEnabled(false);
  }
}

void cmbNucBoundaryLayerWidget::onClear()
{
  this->Core = NULL;
}

void cmbNucBoundaryLayerWidget::checkChange()
{
  if(this->Core == NULL) return;

  bool hasChange = false;
  if(this->Internal->boundaryLayer->isChecked() )
  {
    if(this->Core->getNumberOfBoundaryLayers() != 0)
    {
      cmbNucCore::boundaryLayer * bl = this->Core->getBoundaryLayer(0);
      bool tmpbias = this->Internal->bias->value() != bl->Bias ;
      bool tmpthichness = this->Internal->thickness->value() != bl->Thickness;
      bool tmpinterval = this->Internal->interval->value() != bl->Intervals;
      bool tmpmat =
        cmbNucMaterialColors::instance()->getMaterial(this->Internal->boundaryMaterials)
                                                                        != bl->interface_material;
      if(tmpbias || tmpthichness  || tmpinterval  || tmpmat ) hasChange = true;
    }
    else
    {
      hasChange = true;
    }
  }
  else if(this->Core->getNumberOfBoundaryLayers() != 0) hasChange = true;
  setValueChanged(hasChange);
}

void cmbNucBoundaryLayerWidget::setBoundaryEnabled(bool v)
{
  this->Internal->bias->setVisible(v);
  this->Internal->thickness->setVisible(v);
  this->Internal->interval->setVisible(v);
  this->Internal->boundaryMaterials->setVisible(v);
  this->Internal->biasLabel->setVisible(v);
  this->Internal->thicknessLabel->setVisible(v);
  this->Internal->intervalLabel->setVisible(v);
  this->Internal->materialLabel->setVisible(v);
}
